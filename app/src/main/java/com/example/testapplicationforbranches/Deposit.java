package com.example.testapplicationforbranches;

import android.widget.CheckBox;
import android.widget.EditText;

public class Deposit {


    public static float getFloatValueFromEditText(EditText text) {
        String value = text.getText().toString();
        float number;
        if (value.length() == 0) {
            return number = 0;
        } else {
            number = Float.parseFloat(value);
            return number;
        }
    }

    public static boolean isCapitalizationTrue(CheckBox capitalisation) {
        if (capitalisation.isChecked()) {
            return true;
        } else {
            return false;
        }
    }

    public static String getMonthProfit(EditText sum,
                                        EditText percent,
                                        EditText taxInPercent) {

        float sumValue = getFloatValueFromEditText(sum);
        float percentValue = getFloatValueFromEditText(percent);
        float taxInPercentValue = getFloatValueFromEditText(taxInPercent);

        return String.valueOf(((sumValue * percentValue) / 100 / 12) * (1 - (taxInPercentValue / 100)));
    }

    public static String getTotalSumOfAdditionalPayments(EditText duration,
                                                         EditText eachMonthAdditionalMoney) {
        float durationValue = getFloatValueFromEditText(duration);
        if (durationValue == 0) {
            return String.valueOf(0);
        } else {
            float eachMonthAdditionalMoneyValue = getFloatValueFromEditText(eachMonthAdditionalMoney) - 1;
            return String.valueOf(durationValue * eachMonthAdditionalMoneyValue);
        }
    }

    public static float getTotalCleanProfit_From_Dmitriy(EditText dp_sumValue,
                                                         EditText dp_percentValue,
                                                         EditText dp_durationValue,
                                                         EditText dp_eachMonthAdditionalMoneyValue,
                                                         EditText dp_taxInPercentValue,
                                                         CheckBox dp_capitalisationChkBx) {

        float sumValue = getFloatValueFromEditText(dp_sumValue);
        float percentValue = getFloatValueFromEditText(dp_percentValue);
        float durationValue = getFloatValueFromEditText(dp_durationValue);
        float eachMonthAdditionalMoneyValue = getFloatValueFromEditText(dp_eachMonthAdditionalMoneyValue);
        float taxInPercentValue = getFloatValueFromEditText(dp_taxInPercentValue);
        float result = 0;
        float profit = 0;
        float percentPerMonth = 0;
        float percentPerMonthMinusSum = 0;

        if (isCapitalizationTrue(dp_capitalisationChkBx)) {
            if (durationValue == 1) {
                result = sumValue + (((sumValue * ((percentValue / 12 / 100) + 1)) - sumValue)) * (1 - (taxInPercentValue / 100));
            } else if (durationValue > 1) {
                for (int i = 0; i < durationValue; i++) {
                    profit = (((sumValue + percentPerMonth) + (eachMonthAdditionalMoneyValue * i)) * ((percentValue / 12 / 100) + 1));
                    percentPerMonth = (profit) - (sumValue + (eachMonthAdditionalMoneyValue * i));
                    percentPerMonthMinusSum = percentPerMonth;
                }
                percentPerMonthMinusSum *= (1 - (taxInPercentValue / 100));
                percentPerMonthMinusSum += sumValue + (eachMonthAdditionalMoneyValue * (durationValue - 1));
                result = percentPerMonthMinusSum;
            }
        } else {
            for (int i = 0; i < durationValue; i++) {
                profit = ((((sumValue + (eachMonthAdditionalMoneyValue * i)) * ((percentValue / 12 / 100) + 1)) - (sumValue + (eachMonthAdditionalMoneyValue * i))));
                percentPerMonth += profit;
            }
            result = percentPerMonth * (1 - (taxInPercentValue / 100));
            result += sumValue + (eachMonthAdditionalMoneyValue * (durationValue - 1));
        }
        return result;
    }

    public static float getTotalMontlyCleanProfitMinusDepositBody_From_Dmitriy(EditText dp_sumValue,
                                                                               EditText dp_percentValue,
                                                                               EditText dp_durationValue,
                                                                               EditText dp_eachMonthAdditionalMoneyValue,
                                                                               EditText dp_taxInPercentValue,
                                                                               CheckBox dp_capitalisationChkBx) {

        float sumValue = getFloatValueFromEditText(dp_sumValue);
        float percentValue = getFloatValueFromEditText(dp_percentValue);
        float durationValue = getFloatValueFromEditText(dp_durationValue);
        float eachMonthAdditionalMoneyValue = getFloatValueFromEditText(dp_eachMonthAdditionalMoneyValue);
        float taxInPercentValue = getFloatValueFromEditText(dp_taxInPercentValue);
        float result = 0;
        float profit = 0;
        float percentPerMonth = 0;
        float percentPerMonthMinusSum = 0;

        if (isCapitalizationTrue(dp_capitalisationChkBx)) {
            if (durationValue == 1) {
                result = (((sumValue * ((percentValue / 12 / 100) + 1)) - sumValue)) * (1 - (taxInPercentValue / 100));
            } else if (durationValue > 1) {
                for (int i = 0; i < durationValue; i++) {
                    profit = (((sumValue + percentPerMonth) + (eachMonthAdditionalMoneyValue * i)) * ((percentValue / 12 / 100) + 1));
                    percentPerMonth = (profit) - (sumValue + (eachMonthAdditionalMoneyValue * i));
                    percentPerMonthMinusSum = percentPerMonth;
                }
                percentPerMonthMinusSum *= (1 - (taxInPercentValue / 100));
                result = percentPerMonthMinusSum;
            }
        } else {
            for (int i = 0; i < durationValue; i++) {
                profit = ((((sumValue + (eachMonthAdditionalMoneyValue * i)) * ((percentValue / 12 / 100) + 1)) - (sumValue + (eachMonthAdditionalMoneyValue * i))));
                percentPerMonth += profit;
            }
            result = percentPerMonth * (1 - (taxInPercentValue / 100));
        }
        return result;
    }

}
