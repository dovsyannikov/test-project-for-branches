package com.example.testapplicationforbranches;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnTextChanged;

import static com.example.testapplicationforbranches.Deposit.getMonthProfit;
import static com.example.testapplicationforbranches.Deposit.getTotalCleanProfit_From_Dmitriy;
import static com.example.testapplicationforbranches.Deposit.getTotalMontlyCleanProfitMinusDepositBody_From_Dmitriy;
import static com.example.testapplicationforbranches.Deposit.getTotalSumOfAdditionalPayments;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.dp_sumValue)
    EditText dp_sumValue;
    @BindView(R.id.dp_percentValue)
    EditText dp_percentValue;
    @BindView(R.id.dp_durationValue)
    EditText dp_durationValue;
    @BindView(R.id.dp_capitalisationChkBx)
    CheckBox dp_capitalisationChkBx;
    @BindView(R.id.dp_eachMonthAdditionalMoneyValue)
    EditText dp_eachMonthAdditionalMoneyValue;
    @BindView(R.id.dp_taxInPercentValue)
    EditText dp_taxInPercentValue;
    @BindView(R.id.dp_eachMonthProfitView)
    TextView dp_eachMonthProfitView;
    @BindView(R.id.dp_totalProfitAfterNMonthsView)
    TextView dp_totalProfitAfterNMonthsView;
    @BindView(R.id.dp_totalSumOfAdditionalPaymentsView)
    TextView dp_totalSumOfAdditionalPaymentsView;
    @BindView(R.id.dp_finalResultView)
    TextView dp_finalResultView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setTitle("Deposit Calc");
    }

    @OnTextChanged(R.id.dp_sumValue)
    public void isDPSumValueChangedTextChanged() {
        updateValues();
    }

    @OnTextChanged(R.id.dp_percentValue)
    public void isDPpercentValueTextChanged() {
        updateValues();
    }

    @OnTextChanged(R.id.dp_durationValue)
    public void isDPDurationValueTextChanged() {
        updateValues();
    }

    @OnTextChanged(R.id.dp_eachMonthAdditionalMoneyValue)
    public void isDPEachMonthAdditionalMoneyValueTextChanged() {
        updateValues();
    }

    @OnCheckedChanged(R.id.dp_capitalisationChkBx)
    public void isCheckBoxIsCheckedTextChanged() {
        updateValues();
    }

    @OnTextChanged(R.id.dp_taxInPercentValue)
    public void isDPTaxInPercentValueTextChanged() {
        updateValues();
    }

    public void updateValues() {
        dp_eachMonthProfitView.setText(String.format(Locale.getDefault(), "Eжемесячный доход: %s ", getMonthProfit(dp_sumValue, dp_percentValue, dp_taxInPercentValue)));
        dp_totalProfitAfterNMonthsView.setText(String.format(Locale.getDefault(), "Общий доход через %s мес: %.2f",
                dp_durationValue.getText().toString(), getTotalMontlyCleanProfitMinusDepositBody_From_Dmitriy(dp_sumValue,
                        dp_percentValue,
                        dp_durationValue,
                        dp_eachMonthAdditionalMoneyValue,
                        dp_taxInPercentValue,
                        dp_capitalisationChkBx)));

        dp_totalSumOfAdditionalPaymentsView.setText(String.format("Общая сумма пополнений: %s", getTotalSumOfAdditionalPayments(dp_durationValue, dp_eachMonthAdditionalMoneyValue)));

        dp_finalResultView.setText(String.format(Locale.getDefault(), "Итоговая сумма (процент + вклад): %.2f",
                getTotalCleanProfit_From_Dmitriy(dp_sumValue,
                        dp_percentValue,
                        dp_durationValue,
                        dp_eachMonthAdditionalMoneyValue,
                        dp_taxInPercentValue,
                        dp_capitalisationChkBx)));
    }


}
