package com.example.testapplicationforbranches;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FinanceCalc extends AppCompatActivity {

    @BindView(R.id.fc_deposit)
    Button fc_deposit;
    @BindView(R.id.fc_credit)
    Button fc_credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance_calc);
        ButterKnife.bind(this);
        setTitle("Finance calc");
    }

    @OnClick(R.id.fc_deposit)
    public void onDepositClick() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.fc_credit)
    public void onCreditClick() {
        Intent intent = new Intent(this, CreditCalc.class);
        startActivity(intent);
    }
}
