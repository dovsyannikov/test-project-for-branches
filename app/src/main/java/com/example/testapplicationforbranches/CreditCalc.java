package com.example.testapplicationforbranches;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class CreditCalc extends AppCompatActivity {

    @BindView(R.id.cr_creditSum)
    EditText creditSum;
    @BindView(R.id.cr_creditPercentPerYear)
    EditText creditPercentPerYear;
    @BindView(R.id.cr_creditDurationInMonths)
    EditText creditDurationInMonths;
    @BindView(R.id.cr_creditAssurancePercent)
    EditText creditAssurancePercent;
    @BindView(R.id.cr_creditAvansPayment)
    EditText creditAvansPayment;
    @BindView(R.id.cr_creditComissionPerMonth)
    EditText creditComissionPerMonth;

    @BindView(R.id.cr_creditBody)
    TextView cr_avansPayment;
    @BindView(R.id.cr_creditMonthPayment)
    TextView cr_monthlyPayment;
    @BindView(R.id.cr_creditFullOvercharge)
    TextView cr_creditFullOvercharge;
    @BindView(R.id.cr_creditAmountOfCreditClosing)
    TextView cr_creditAmountOfCreditClosing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_calc);
        ButterKnife.bind(this);

        setTitle("Credit calc");
    }

    @OnTextChanged(R.id.cr_creditSum)
    public void onCreditSumChanged() {
        updateFields();
    }

    @OnTextChanged(R.id.cr_creditPercentPerYear)
    public void onPercentPerYearChanged() {
        updateFields();
    }

    @OnTextChanged(R.id.cr_creditDurationInMonths)
    public void onDurationInMonthsChanged() {
        updateFields();
    }

    @OnTextChanged(R.id.cr_creditAssurancePercent)
    public void onAssurancePercentChanged() {
        updateFields();
    }

    @OnTextChanged(R.id.cr_creditAvansPayment)
    public void onAvansPaymentChanged() {
        updateFields();
    }

    @OnTextChanged(R.id.cr_creditComissionPerMonth)
    public void onComissionPerMonthChanged() {
        updateFields();
    }

    public void updateFields() {
        cr_avansPayment.setText(String.format(Locale.getDefault(), "Первоначальный взнос: %.2f", Credit.getAvansSum(creditSum, creditAvansPayment)));
        cr_monthlyPayment.setText(String.format(Locale.getDefault(), "Ежемесячный платеж: %.2f", Credit.getCreditMonthPayment(creditSum,
                creditPercentPerYear,
                creditDurationInMonths,
                creditAssurancePercent,
                creditComissionPerMonth,
                creditAvansPayment)));
        cr_creditFullOvercharge.setText(String.format(Locale.getDefault(), "Общая переплата по кредиту: %.2f", Credit.getCreditFullOvercharge(creditSum,
                creditPercentPerYear,
                creditDurationInMonths,
                creditAssurancePercent,
                creditComissionPerMonth,
                creditAvansPayment)));
        cr_creditAmountOfCreditClosing.setText(String.format(Locale.getDefault(), "Итоговая сумма погашения: %.2f", Credit.getCreditAmountOfCreditClosing(creditSum,
                creditPercentPerYear,
                creditDurationInMonths,
                creditAssurancePercent,
                creditComissionPerMonth,
                creditAvansPayment)));
    }

}
