package com.example.testapplicationforbranches;

import android.widget.EditText;

public class Credit {

    public static float getFloatValueFromEditText(EditText text) {
        String value = text.getText().toString();
        float number;
        if (value.length() == 0) {
            return number = 0;
        } else {
            number = Float.parseFloat(value);
            return number;
        }
    }

    public static float getAvansSum(EditText creditSum,
                                    EditText creditAvansPayment) {
        float sum = getFloatValueFromEditText(creditSum);
        float avansPayment = getFloatValueFromEditText(creditAvansPayment);
        return ((sum * avansPayment) / 100);
    }

    public static float getCreditMonthPayment(EditText creditSum,
                                              EditText creditPercentPerYear,
                                              EditText creditDurationInMonths,
                                              EditText creditAssurancePercent,
                                              EditText creditComissionPerMonth,
                                              EditText creditAvansPayment) {
        float sum = getFloatValueFromEditText(creditSum);
        float percentPerYear = getFloatValueFromEditText(creditPercentPerYear);
        float durationInMonths = getFloatValueFromEditText(creditDurationInMonths);
        float assurancePercent = getFloatValueFromEditText(creditAssurancePercent);
        float comissionPerMonth = getFloatValueFromEditText(creditComissionPerMonth);
        float avansPayment = getFloatValueFromEditText(creditAvansPayment);

        float sumWithoutAvans = sum * (1 - avansPayment / 100);

        return ((sumWithoutAvans / 100) * ((100 / durationInMonths) + (percentPerYear / 12) + assurancePercent / 12 + comissionPerMonth));
    }

    public static float getCreditFullOvercharge(EditText creditSum,
                                                EditText creditPercentPerYear,
                                                EditText creditDurationInMonths,
                                                EditText creditAssurancePercent,
                                                EditText creditComissionPerMonth,
                                                EditText creditAvansPayment) {
        float sum = getFloatValueFromEditText(creditSum);
        float percentPerYear = getFloatValueFromEditText(creditPercentPerYear);
        float durationInMonths = getFloatValueFromEditText(creditDurationInMonths);
        float assurancePercent = getFloatValueFromEditText(creditAssurancePercent);
        float comissionPerMonth = getFloatValueFromEditText(creditComissionPerMonth);
        float avansPayment = getFloatValueFromEditText(creditAvansPayment);

        float sumWithoutAvans = sum * (1 - avansPayment / 100);

        return ((sumWithoutAvans / 100) * (((percentPerYear * durationInMonths) / 12) + assurancePercent / 12 + (comissionPerMonth * durationInMonths)));
    }

    public static float getCreditAmountOfCreditClosing(EditText creditSum,
                                                       EditText creditPercentPerYear,
                                                       EditText creditDurationInMonths,
                                                       EditText creditAssurancePercent,
                                                       EditText creditComissionPerMonth,
                                                       EditText creditAvansPayment) {

        float sum = getFloatValueFromEditText(creditSum);
        float avansPayment = getFloatValueFromEditText(creditAvansPayment);
        float numberCreditFullOvercharge = getCreditFullOvercharge(creditSum,
                creditPercentPerYear,
                creditDurationInMonths,
                creditAssurancePercent,
                creditComissionPerMonth,
                creditAvansPayment);

        float sumWithoutAvans = sum * (1 - avansPayment / 100);
        return (sumWithoutAvans + numberCreditFullOvercharge);
    }

}
